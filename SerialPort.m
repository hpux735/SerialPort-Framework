//
//  SerialPort.m
//  SerialFramework
//
//  Created by William Dillon on 10/23/08.
//  Copyright 2008 Oregon State University. All rights reserved.
//

#import "SerialPort.h"
#include <sys/ioctl.h>

#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>
#if defined(MAC_OS_X_VERSION_10_3) && (MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_3)
#include <IOKit/serial/ioss.h>
#endif
#include <IOKit/IOBSD.h>

#define SERIAL_PORT_PARITY_MASK		0x38
#define SERIAL_PORT_DATABIT_MASK	0x06
#define SERIAL_PORT_STOPBIT_MASK	0x01

@implementation SerialPort

static bool error;

NSArray *fillArrayWithPorts(io_iterator_t *services)
{
	io_object_t		modemService;
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
		// Iterate across all modems found. In this example, we bail after finding the first modem.    
    while ((modemService = IOIteratorNext(*services)))
    {
        CFTypeRef	bsdPathAsCFString;
		CFTypeRef	nameAsCFString;
		
			// Get the dialin device's path (/dev/tty.xxxxx).
		bsdPathAsCFString = IORegistryEntryCreateCFProperty(modemService,
                                                            CFSTR(kIODialinDeviceKey),
                                                            kCFAllocatorDefault,
                                                            0);

			// Get the device name
		nameAsCFString = IORegistryEntryCreateCFProperty(modemService,
                                                            CFSTR(kIOTTYDeviceKey),
                                                            kCFAllocatorDefault,
                                                            0);
		
		if (bsdPathAsCFString && nameAsCFString)
        {
				// Treat the CFStrings as NSString, and add them to a dict
				// one per device.  Add these to an array of serial devices
			[tempArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
								  [[(NSString *)bsdPathAsCFString copy] autorelease], @"path",
								  [[(NSString *)nameAsCFString copy] autorelease], @"name", nil]];
			
        }
        
        if (bsdPathAsCFString) CFRelease(bsdPathAsCFString);
        if (nameAsCFString)    CFRelease(nameAsCFString);		
		IOObjectRelease(modemService);
    }
	
	NSArray *outputArray = [tempArray copy];
	[tempArray release];
	[outputArray autorelease];
	return outputArray;
}

+ (NSArray *)findSerialPorts
{
	kern_return_t           kernResult; 
    CFMutableDictionaryRef  classesToMatch;
	io_iterator_t			matchingServices;
	
    // Serial devices are instances of class IOSerialBSDClient
    classesToMatch = IOServiceMatching(kIOSerialBSDServiceValue);
    if (classesToMatch == NULL) {
        printf("IOServiceMatching returned a NULL dictionary.\n");
		goto exit;
    } else {
        CFDictionarySetValue(classesToMatch,
                             CFSTR(kIOSerialBSDTypeKey),
                             CFSTR(kIOSerialBSDRS232Type));        
    }
    
    kernResult = IOServiceGetMatchingServices(kIOMasterPortDefault, classesToMatch, &matchingServices);
    if (KERN_SUCCESS != kernResult)
    {
        printf("IOServiceGetMatchingServices returned %d\n", kernResult);
        goto exit;
    }
	
	return fillArrayWithPorts(&matchingServices);
	
exit:
    return nil;
}

- (id)init:(NSString *)fileName
 withSpeed:(int)speed
  andFlags:(int)newFlags
{
	self = [super init];
	
	if( self == nil ) {
		return nil;
	}
	
	struct termios tty;

	fprintf(stderr, "Attempting to open tty: %s\n", [fileName cStringUsingEncoding:NSUTF8StringEncoding]);
	
	// Must open the TTY "non-blocking"
	serialFD = open([fileName cStringUsingEncoding:NSUTF8StringEncoding], O_RDWR | O_NOCTTY | O_NONBLOCK );
	if( serialFD < 0 ) {
		perror("Opening file");
		goto error;
	}
	
	if( ioctl(serialFD, TIOCEXCL) == -1 ) {
		perror("Setting exclusive access to serial port");
		goto error;
	}
	
	if( fcntl(serialFD, F_SETFL, 0) != 0 ) {
		perror("Enable blocking I/O");
		goto error;
	}
	
	fprintf(stderr, "Opened tty device file descriptor: %d.\n", serialFD);
	
	if( tcgetattr(serialFD, &orig) != 0 ) {
		perror("Getting serial settings");
		goto error;
	}
	
	// Copy original settings to copy defaults
	tty = orig;
	
	tty.c_iflag = IGNBRK;	// Ignore breaks and parity errors
	tty.c_oflag = 0;		// No special output needs
	tty.c_lflag = 0;		// No local setting needs
	tty.c_cflag = CREAD;	// Enable receiver 
	
    // Set the raw input (non-canonical) mode, where reads block until
    // at least one character is received, or a second elapses.
	cfmakeraw(&tty);
	tty.c_cc[VMIN]	= 1;
	tty.c_cc[VTIME]	= 10;
	
	// Set the number of databits from the flags
	switch(newFlags & SERIAL_PORT_DATABIT_MASK) {
		case SERIAL_PORT_5BIT:
			tty.c_cflag |= CS5;
			break;
		case SERIAL_PORT_6BIT:
			tty.c_cflag |= CS6;
			break;
		case SERIAL_PORT_7BIT:
			tty.c_cflag |= CS7;
			break;
		case SERIAL_PORT_8BIT:
			tty.c_cflag |= CS8;
	}
	
	// Set the parity from flag bits (mark and space not yet implemented)
	switch(newFlags & SERIAL_PORT_PARITY_MASK) {
		case SERIAL_PORT_EVEN:
			tty.c_cflag |= PARENB;
			break;
		case SERIAL_PORT_ODD:
			tty.c_cflag |= PARENB | PARODD;
			break;
		case SERIAL_PORT_NONE:
			tty.c_iflag |= IGNPAR;
			break;
		default:
			NSLog(@"Parity setting not yet implemented or invalid.\n");
	}
	
	if( !(newFlags & SERIAL_PORT_HWFLC) ) {
		tty.c_cflag |= CLOCAL;	// Disable HW flow control
	} else {
		tty.c_cflag |= CCTS_OFLOW | CRTS_IFLOW;
        printf("Set hardware flow control\n");
	}
	
	if( newFlags & SERIAL_PORT_STOPBIT_MASK ) {
		tty.c_cflag |= CSTOPB;	// Send 2 stop bits
	}

	fflush(stdout);
    
	if( cfsetspeed(&tty, speed) != 0 ) {
		NSLog(@"Setting speed to %d baud: %s", speed, strerror(errno));
		goto error;
	}
	
    printf("Set baud rate to %d\n", (int) cfgetispeed(&tty));
	fflush(stdout);

	if( tcsetattr(serialFD, TCSANOW, &tty) != 0 ) {
		perror("Setting serial port attributes");
		goto error;
	}

    printf("Set flags.\n");
	fflush(stdout);
    
	// Get the 'BEFORE' line bits
	ioctl(serialFD, TIOCMGET, &flags);
	fprintf(stderr, "Flags are 0x%x.\n", flags);
	fflush(stdout);
	
	return self;
	
error:
	[self release];
	self = nil;
	return self;
}

- (void)dealloc {
	NSLog(@"Closing serial port");
	
		// Drain the buffers
	if (tcdrain(serialFD) == -1) {
		perror("Waiting for drain");
	}
	
		// Return the port to previous settings
	if (tcsetattr(serialFD, TCSANOW, &orig) == -1) {
		perror("Restoring original port settings");
	}
	
	close(serialFD);
		// Close the serial port descriptor
	
    [super dealloc];
}

- (char)getByte
{
	int retval;
	char temp;
	
	while( (retval = read(serialFD, &temp, 1)) == 0 ) {
		NSLog(@"Zero-length read.");
	}
	
	if( retval < 0 ) {
		perror("Filling buffer");
		error = YES;
	}

	error = NO;
	return temp;
}

- (NSData *)getData:(int)count
{
	int retval, bytes = 0;
	
	char *buffer = (char *)malloc(count);
	
	// Fill the Buffer:
	while( bytes < count ) {
//		while( (retval = read(serialFD, &buffer[bytes], count-bytes)) == 0 );
		while( (retval = read(serialFD, &buffer[bytes], 1)) == 0 );
		
		if( retval > 0 ) {
			bytes += retval;				
		} else {
			perror("Filling buffer");
		}		
	}
	
	NSData *temp = [NSData dataWithBytes:buffer length:count];
	
	free(buffer);
	
	return temp;
}

- (bool)putData:(NSData *)data
{
	int retval;
	unsigned int bytes = 0;
	
	// Fill the Buffer:
	while( bytes < [data length] ) {
		while( (retval = write(serialFD, &((char *)[data bytes])[bytes], [data length]-bytes)) == 0 );
		
		if( retval > 0 ) {
			bytes += retval;				
		} else {
			perror("Writing data");
			return NO;
		}		
	}
	
	return YES;
}

-(_Bool)putString:(NSString *)string
{
    return [self putData:[string dataUsingEncoding:NSUTF8StringEncoding]];
}

- (bool)putByte:(char)byte
{
	if( write(serialFD, &byte, 1) != 1 ) {
		perror("Writing byte to serial port");
		return NO;
	}
	
	return YES;
}

- (void)setRTS:(bool)state
{
	int temp = flags;
// Set or clear RTS according to the desired state
	
	if(state) {
		temp	|=  TIOCM_RTS;
	} else {
		temp	&= ~TIOCM_RTS;	
	}
	
	ioctl(serialFD, TIOCMSET, &temp);
	fprintf(stderr, "Setting %x.\n", temp);
}

- (void)resetRTS
{
// Reset to the 'BEFORE' state
	ioctl(serialFD, TIOCMSET, &flags);
	fprintf(stderr, "Setting %x.\n", flags);
}

- (NSString *)getLine
{
	NSMutableString *string, *returnString;
	char buffer[257];
	int i = 0;
	
	buffer[256] = '\0';
	
	string = [[NSMutableString alloc] initWithCapacity:256];
	
	do {
		buffer[i++] = [self getByte];
		
		if (error == YES) {
			NSLog(@"Error reading line.");
			[string release];
			return nil;
		}
		
//		printf("%c", buffer[i-1]);
		
		// Store the buffer into the NSString and reset
		if( i == 256 ) {
			[string appendFormat:@"%s", buffer];
			buffer[0] = buffer[i-1];
			i = 0;
		}
		
	} while((buffer[i-1] != '\n') && 
			(buffer[i-1] != '\r') );
	
	buffer[i-1] = '\0';
	
	[string appendFormat:@"%s", buffer];
	returnString = [NSString stringWithString:string];
	[string release];
	
	return returnString;
}

- (void)sendBreak
{
    tcsendbreak(serialFD, 0);
}

- (int)serialFD
{
	return serialFD;
}
//@synthesize serialFD;


@end
