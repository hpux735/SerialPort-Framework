//
//  SerialPort.h
//  SerialFramework
//
//  Created by William Dillon on 10/23/08.
//  Copyright 2008 Oregon State University. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <termios.h>

//	Common defaults 
#define SERIAL_PORT_N81	0x00	// Default (no flow control)
#define SERIAL_PORT_E71	0x0C	// Default (no flow control)

//	All options (bit-wise 'or'd)
#define SERIAL_PORT_NONE  0x00
#define SERIAL_PORT_EVEN  0x08
#define SERIAL_PORT_ODD   0x10
#define SERIAL_PORT_MARK  0x18
#define SERIAL_PORT_SPACE 0x20

#define SERIAL_PORT_8BIT  0x00
#define SERIAL_PORT_5BIT  0x02
#define SERIAL_PORT_6BIT  0x04
#define SERIAL_PORT_7BIT  0x06

#define SERIAL_PORT_1STOP 0x00
#define SERIAL_PORT_2STOP 0x01

#define SERIAL_PORT_HWFLC 0x40

@interface SerialPort : NSObject {
	struct termios orig;
	int			   flags;
	int			   serialFD;
}

//@property(readonly) int serialFD;

// Class method to find all available serial ports
+ (NSArray *)findSerialPorts;

- (id)init:(NSString *)fileName withSpeed:(int)portSpeed andFlags:(int)flags;

- (char)getByte;
- (bool)putByte:(char)byte;

- (NSData *)getData:(int)bytes;
- (bool)putData:(NSData *)data;

- (NSString *)getLine;
- (bool)putString:(NSString *)string;

- (void)setRTS:(bool)state;
- (void)resetRTS;

- (void)sendBreak;

- (int)serialFD;

@end
